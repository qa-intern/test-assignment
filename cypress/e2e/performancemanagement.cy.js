describe('Login Module', () => {
    it('should log in with valid credentials', () => {
        cy.visit('https://opensource-demo.orangehrmlive.com/web/index.php/auth/login');
        cy.get(':nth-child(2) > .oxd-input-group > :nth-child(2) > .oxd-input').type('Admin')
        cy.get(':nth-child(3) > .oxd-input-group > :nth-child(2) > .oxd-input').type('admin123')
        cy.get('button[type="submit"]').click();
        cy.url().should('include', '/dashboard');
        cy.get('.orangehrm-todo-list > :nth-child(1)').click()
    });
});
      

   