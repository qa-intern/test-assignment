describe('Add employee management module', () => {
    it('should log in with valid credentials', () => {
        cy.visit('https://opensource-demo.orangehrmlive.com/web/index.php/auth/login');
        cy.get('input[name="username"]').type('Admin');
        cy.get('input[name="password"]').type('admin123');
        cy.get('button[type="submit"]').click();
        cy.visit('https://opensource-demo.orangehrmlive.com/web/index.php/pim/addEmployee')
        cy.get('.--name-grouped-field > :nth-child(1) > :nth-child(2) > .oxd-input').should('be.visible')
        // cy.get('.--name-grouped-field > :nth-child(1) > :nth-child(2) > .oxd-input')
        // cy.get(':nth-child(3) > :nth-child(2) > .oxd-input').type('mary')
        // cy.get('.oxd-grid-item > .oxd-input-group > :nth-child(2) > .oxd-input').type('0987')
        // cy.get('.oxd-button--secondary').click()
        

    });
}) 